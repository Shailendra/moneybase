//
//  BaseVC.swift
//  MoneyBaseDemo
//
//  Created by Shailendra on 02/01/23.
//

import UIKit

class BaseVC: UIViewController {

    //MARK: - PROPERTIES
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - POP
    internal func POP(_ animated : Bool = true){
        self.navigationController?.popViewController(animated: animated)
    }
    
    //MARK: - DISMISS
    internal func DISMISS(_ animated : Bool = true){
        self.dismiss(animated: animated, completion: nil)
    }
    
    public func moveToReplyScreen(result : Result){
        let vc = UIStoryboard(name: Storyboard.Main.rawValue, bundle: nil).instantiateViewController(withIdentifier: IdentifireName.kDetailsVC) as! DetailsVC
        vc.viewModel.model.result = result
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func showAlert(message : String){
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: .kOK, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
