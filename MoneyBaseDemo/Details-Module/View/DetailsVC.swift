//
//  DetailsVC.swift
//  MoneyBaseDemo
//
//  Created by Shailendra on 02/01/23.
//

import UIKit
import RxSwift
import RxCocoa

class DetailsVC: BaseVC {

    //MARK: - PROPERTIES
    var viewModel = {
        DetailsVM()
    }()
    
    @IBOutlet weak var ExchangeNameLabel      : UILabel!{
        didSet{
            self.ExchangeNameLabel.text = self.viewModel.model.result.fullExchangeName ?? .kEmpty
        }
    }
    @IBOutlet weak var ShortNameLabel         : UILabel!{
        didSet{
            self.ShortNameLabel.text = self.viewModel.model.result.shortName ?? .kEmpty
        }
    }
    @IBOutlet weak var TimeZoneLabel          : UILabel!{
        didSet{
            self.TimeZoneLabel.text = self.viewModel.model.result.exchangeTimezoneName ?? .kEmpty
        }
    }
    @IBOutlet weak var SymbolLabel            : UILabel!{
        didSet{
            self.SymbolLabel.text = self.viewModel.model.result.symbol ?? .kEmpty
        }
    }
    @IBOutlet weak var LanguageLabel          : UILabel!{
        didSet{
            self.LanguageLabel.text = self.viewModel.model.result.language ?? .kEmpty
        }
    }
    @IBOutlet weak var MarketTimeLabel        : UILabel!{
        didSet{
            self.MarketTimeLabel.text = self.viewModel.model.result.customPriceAlertConfidence ?? .kEmpty
        }
    }
    @IBOutlet weak var ShortTimeZoneLabel     : UILabel!{
        didSet{
            self.ShortTimeZoneLabel.text = self.viewModel.model.result.exchangeTimezoneShortName ?? .kEmpty
        }
    }
    @IBOutlet weak var QuoteTypeLabel         : UILabel!{
        didSet{
            self.QuoteTypeLabel.text = self.viewModel.model.result.quoteType ?? .kEmpty
        }
    }
    @IBOutlet weak var RegularMarketTimeLabel : UILabel!{
        didSet{
            self.RegularMarketTimeLabel.text = self.viewModel.model.result.regularMarketTime?.fmt ?? .kEmpty
        }
    }
    @IBOutlet weak var MarketLabel            : UILabel!{
        didSet{
            self.MarketLabel.text = self.viewModel.model.result.market ?? .kEmpty
        }
    }
    @IBOutlet weak var PriceHintLabel         : UILabel!{
        didSet{
            self.PriceHintLabel.text = "\(self.viewModel.model.result.priceHint ?? 0)"
        }
    }
    @IBOutlet weak var SourceIntervalLabel    : UILabel!{
        didSet{
            self.SourceIntervalLabel.text = "\(self.viewModel.model.result.sourceInterval ?? 0)"
        }
    }
    @IBOutlet weak var ExchangeLabel          : UILabel!{
        didSet{
            self.ExchangeLabel.text = self.viewModel.model.result.exchange ?? .kEmpty
        }
    }
    @IBOutlet weak var ExchangeShortNameLabel : UILabel!{
        didSet{
            self.ExchangeShortNameLabel.text = self.viewModel.model.result.exchangeTimezoneShortName ?? .kEmpty
        }
    }
    @IBOutlet weak var RegionLabel            : UILabel!{
        didSet{
            self.RegionLabel.text = self.viewModel.model.result.region ?? .kEmpty
        }
    }
    @IBOutlet weak var PreviousCloseLabel     : UILabel! {
        didSet{
            self.PreviousCloseLabel.text = self.viewModel.model.result.regularMarketPreviousClose?.fmt ?? .kEmpty
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
