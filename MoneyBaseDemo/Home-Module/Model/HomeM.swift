//
//  HomeM.swift
//  MoneyBaseDemo
//
//  Created by Shailendra on 02/01/23.
//

import Foundation

struct MarketCodable : Codable {
    
    let marketSummaryAndSparkResponse : MarketSummaryAndSparkResponse?
    
    enum CodingKeys: String, CodingKey {
        case marketSummaryAndSparkResponse = "marketSummaryAndSparkResponse"
    }
    
    init(from decoder: Decoder) throws {
        let values                         = try decoder.container(keyedBy: CodingKeys.self)
        self.marketSummaryAndSparkResponse = try values.decodeIfPresent(MarketSummaryAndSparkResponse.self, forKey: .marketSummaryAndSparkResponse)
    }
}

struct MarketSummaryAndSparkResponse : Codable {
    
    let result : [Result]?
    let error  : String?
    
    enum CodingKeys: String, CodingKey {
        
        case result = "result"
        case error  = "error"
    }
    
    init(from decoder: Decoder) throws {
        let values  = try decoder.container(keyedBy: CodingKeys.self)
        self.result = try values.decodeIfPresent([Result].self, forKey: .result)
        self.error  = try values.decodeIfPresent(String.self, forKey: .error)
    }
}

struct Result : Codable {
    
    let exchangeTimezoneName       : String?
    let fullExchangeName           : String?
    let symbol                     : String?
    let gmtOffSetMilliseconds      : Int?
    let cryptoTradeable            : Bool?
    let exchangeDataDelayedBy      : Int?
    let firstTradeDateMilliseconds : Int?
    let language                   : String?
    let regularMarketTime          : RegularMarketTime?
    let exchangeTimezoneShortName  : String?
    let quoteType                  : String?
    let marketState                : String?
    let customPriceAlertConfidence : String?
    let market                     : String?
    let priceHint                  : Int?
    let tradeable                  : Bool?
    let sourceInterval             : Int?
    let exchange                   : String?
    let region                     : String?
    let shortName                  : String?
    let regularMarketPreviousClose : RegularMarketPreviousClose?
    let triggerable                : Bool?
    
    enum CodingKeys: String, CodingKey {
        
        case exchangeTimezoneName       = "exchangeTimezoneName"
        case fullExchangeName           = "fullExchangeName"
        case symbol                     = "symbol"
        case gmtOffSetMilliseconds      = "gmtOffSetMilliseconds"
        case cryptoTradeable            = "cryptoTradeable"
        case exchangeDataDelayedBy      = "exchangeDataDelayedBy"
        case firstTradeDateMilliseconds = "firstTradeDateMilliseconds"
        case language                   = "language"
        case regularMarketTime          = "regularMarketTime"
        case exchangeTimezoneShortName  = "exchangeTimezoneShortName"
        case quoteType                  = "quoteType"
        case marketState                = "marketState"
        case customPriceAlertConfidence = "customPriceAlertConfidence"
        case market                     = "market"
        case priceHint                  = "priceHint"
        case tradeable                  = "tradeable"
        case sourceInterval             = "sourceInterval"
        case exchange                   = "exchange"
        case region                     = "region"
        case shortName                  = "shortName"
        case regularMarketPreviousClose = "regularMarketPreviousClose"
        case triggerable                = "triggerable"
    }
    
    init(from decoder: Decoder) throws {
        let values                      = try decoder.container(keyedBy: CodingKeys.self)
        self.exchangeTimezoneName       = try values.decodeIfPresent(String.self, forKey: .exchangeTimezoneName)
        self.fullExchangeName           = try values.decodeIfPresent(String.self, forKey: .fullExchangeName)
        self.symbol                     = try values.decodeIfPresent(String.self, forKey: .symbol)
        self.gmtOffSetMilliseconds      = try values.decodeIfPresent(Int.self, forKey: .gmtOffSetMilliseconds)
        self.cryptoTradeable            = try values.decodeIfPresent(Bool.self, forKey: .cryptoTradeable)
        self.exchangeDataDelayedBy      = try values.decodeIfPresent(Int.self, forKey: .exchangeDataDelayedBy)
        self.firstTradeDateMilliseconds = try values.decodeIfPresent(Int.self, forKey: .firstTradeDateMilliseconds)
        self.language                   = try values.decodeIfPresent(String.self, forKey: .language)
        self.regularMarketTime          = try values.decodeIfPresent(RegularMarketTime.self, forKey: .regularMarketTime)
        self.exchangeTimezoneShortName  = try values.decodeIfPresent(String.self, forKey: .exchangeTimezoneShortName)
        self.quoteType                  = try values.decodeIfPresent(String.self, forKey: .quoteType)
        self.marketState                = try values.decodeIfPresent(String.self, forKey: .marketState)
        self.customPriceAlertConfidence = try values.decodeIfPresent(String.self, forKey: .customPriceAlertConfidence)
        self.market                     = try values.decodeIfPresent(String.self, forKey: .market)
        self.priceHint                  = try values.decodeIfPresent(Int.self, forKey: .priceHint)
        self.tradeable                  = try values.decodeIfPresent(Bool.self, forKey: .tradeable)
        self.sourceInterval             = try values.decodeIfPresent(Int.self, forKey: .sourceInterval)
        self.exchange                   = try values.decodeIfPresent(String.self, forKey: .exchange)
        self.region                     = try values.decodeIfPresent(String.self, forKey: .region)
        self.shortName                  = try values.decodeIfPresent(String.self, forKey: .shortName)
        self.regularMarketPreviousClose = try values.decodeIfPresent(RegularMarketPreviousClose.self, forKey: .regularMarketPreviousClose)
        self.triggerable                = try values.decodeIfPresent(Bool.self, forKey: .triggerable)
    }
}

struct Spark : Codable {
    let timestamp          : [Int]?
    let dataGranularity    : Int?
    let symbol             : String?
    let previousClose      : Double?
    let chartPreviousClose : Double?
    let end                : Int?
    let start              : Int?
    let close              : [Double]?
    
    enum CodingKeys: String, CodingKey {
        
        case timestamp          = "timestamp"
        case dataGranularity    = "dataGranularity"
        case symbol             = "symbol"
        case previousClose      = "previousClose"
        case chartPreviousClose = "chartPreviousClose"
        case end                = "end"
        case start              = "start"
        case close              = "close"
    }
    
    init(from decoder: Decoder) throws {
        let values              = try decoder.container(keyedBy: CodingKeys.self)
        self.timestamp          = try values.decodeIfPresent([Int].self, forKey: .timestamp)
        self.dataGranularity    = try values.decodeIfPresent(Int.self, forKey: .dataGranularity)
        self.symbol             = try values.decodeIfPresent(String.self, forKey: .symbol)
        self.previousClose      = try values.decodeIfPresent(Double.self, forKey: .previousClose)
        self.chartPreviousClose = try values.decodeIfPresent(Double.self, forKey: .chartPreviousClose)
        self.end                = try values.decodeIfPresent(Int.self, forKey: .end)
        self.start              = try values.decodeIfPresent(Int.self, forKey: .start)
        self.close              = try values.decodeIfPresent([Double].self, forKey: .close)
    }
}
struct RegularMarketPreviousClose : Codable {
    
    let raw : Double?
    let fmt : String?
    
    enum CodingKeys: String, CodingKey {
        
        case raw = "raw"
        case fmt = "fmt"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.raw   = try values.decodeIfPresent(Double.self, forKey: .raw)
        self.fmt   = try values.decodeIfPresent(String.self, forKey: .fmt)
    }
}

struct RegularMarketTime : Codable {
    
    let raw : Int?
    let fmt : String?
    
    enum CodingKeys: String, CodingKey {
        
        case raw = "raw"
        case fmt = "fmt"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.raw   = try values.decodeIfPresent(Int.self, forKey: .raw)
        self.fmt   = try values.decodeIfPresent(String.self, forKey: .fmt)
    }
}
