//
//  HomeCell.swift
//  MoneyBaseDemo
//
//  Created by Shailendra on 02/01/23.
//

import UIKit

class HomeCell: UITableViewCell {

    //MARK: - PROPETIES
    @IBOutlet weak var exchangeNameLabel : UILabel!
    @IBOutlet weak var shortNameLabel    : UILabel!
    @IBOutlet weak var timeZoneLabel     : UILabel!
    
    class var identifier: String { return String(describing: self) }
    class var nib: UINib { return UINib(nibName: identifier, bundle: nil) }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
