//
//  ViewController.swift
//  MoneyBaseDemo
//
//  Created by Shailendra on 02/01/23.
//

import UIKit
import RxCocoa
import RxSwift

class ViewController: BaseVC,UISearchBarDelegate {
    
    //MARK: - PROPERTIES
    @IBOutlet weak var homeTableView   : UITableView!
    @IBOutlet weak var searchBar       : UISearchBar!
    private let disposeBag             = DisposeBag()
    var filterResult: BehaviorRelay<[Result]> = BehaviorRelay(value: [])
    let searchText = BehaviorRelay<String>(value: .kEmpty)
    var viewModel = HomeVM()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCell()
        self.setUpViewBindings()
       self.addSearchBarObserver()
        Observable<Int>.interval(.seconds(8), scheduler: MainScheduler.instance).bind { timePassed in
            self.setUpSubscription()
        }.disposed(by: disposeBag)
    }
    
    //MARK: - REGISTER CELL
    private func registerCell(){
        self.homeTableView.register(HomeCell.nib, forCellReuseIdentifier: HomeCell.identifier)
    }
    
    //MARK: - SETUP VIEW BINDINGS
    func setUpViewBindings() {
        
        self.viewModel.dataSource.bind(to: self.homeTableView.rx.items) { (tableView, row, element) in
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: HomeCell.identifier)! as! HomeCell
            cell.exchangeNameLabel.text = element.fullExchangeName ?? .kEmpty
            cell.shortNameLabel.text    = element.shortName ?? .kEmpty
            cell.timeZoneLabel.text     = element.exchangeTimezoneName ?? .kEmpty
            return cell
        }
        .disposed(by: self.disposeBag)
        
       self.homeTableView.rx.itemSelected.subscribe(onNext: { [weak self] indexPath in
            guard let self = self else {
                return
            }
            self.homeTableView.deselectRow(at: indexPath, animated: true)
            let result : Result = try! self.homeTableView.rx.model(at: indexPath)
            self.moveToReplyScreen(result: result)
        }).disposed(by: disposeBag)
        
        Observable.combineLatest(self.viewModel.dataSource.asObservable(), searchText) { items, query in
            return items.filter({ item in
                item.fullExchangeName!.lowercased().contains(query.lowercased())
            })
        }.subscribe(onNext: { resultArray in
            if !resultArray.isEmpty {
                self.filterResult.accept(resultArray)
                DispatchQueue.main.async {
                    self.homeTableView.reloadData()
                }
            }
        })
        .disposed(by: disposeBag)
    }
    
    //MARK: - SETUP SUBSRIPTION
    func setUpSubscription() {
        self.viewModel.callAPIFromViewModel(url: kBaseURL)
    }
    
    //MARK: - ADD SEARCH BAR OBSERVER
    private func addSearchBarObserver() {
        self.searchBar
            .rx
            .text
            .orEmpty
            .debounce(.milliseconds(500), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe { [weak self] query in
                guard
                    let query = query.element else { return }
                self?.searchText.accept(query)
            }
            .disposed(by: disposeBag)
    }
}
