//
//  HomeVM.swift
//  MoneyBaseDemo
//
//  Created by Shailendra on 02/01/23.
//

import Foundation
import RxSwift
import RxCocoa

class HomeVM {
    
    var dataSource: BehaviorRelay<[Result]> = BehaviorRelay(value: [])
    var error : Error?
    let disposeBag = DisposeBag()
    
    public func callAPIFromViewModel(url : String) {
        
        let disposable = APIHandler.init().callAPIFromApiHandler(url : url).subscribe(
            onNext: { (data) in
                let jsonDcoderObj = JSONDecoder.init()
                do {
                    let model = try jsonDcoderObj.decode(MarketCodable.self, from: data!)
                    self.dataSource.accept((model.marketSummaryAndSparkResponse?.result) ?? [])
                } catch {
                    self.error = error
                }
            },
            onError: { (error) in self.error = error },
            onCompleted: { }) { print("Disposed") }
        disposable.disposed(by: disposeBag)
    }
}

