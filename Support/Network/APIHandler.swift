//
//  WebService.swift
//  MoneyBaseDemo
//
//  Created by Shailendra on 02/01/23.
//

import Foundation
import RxSwift
import RxCocoa


class APIHandler : APIHandlerDelegate{
    
    func callAPIFromApiHandler(url : String)
    -> Observable<Data?> {
        
        Observable<Data?>.create { observer  in
            var request = URLRequest(url: URL(string: url)!)
            request.httpMethod = RequestType.GET.rawValue
            let headers = [
                "X-RapidAPI-Key"  : "7e889b7781mshf8fda31c2ebc3ffp129ae9jsn6418858c86b5",
                "X-RapidAPI-Host" : "yh-finance.p.rapidapi.com"
            ]
            request.allHTTPHeaderFields = headers
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                observer.onNext(data)
                if error != nil {
                    observer.onError(error!)
                }
                observer.onCompleted()
                
            }.resume()
            let disposable = Disposables.create()
            return disposable
        }
    }
}
