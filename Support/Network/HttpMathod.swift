//
//  HttpMathod.swift
//  MoneyBaseDemo
//
//  Created by Shailendra on 02/01/23.
//

import Foundation

enum RequestType : String {
    case GET    = "GET"
    case POST   = "POST"
    case PUT    = "PUT"
    case DELETE = "DELETE"
}
