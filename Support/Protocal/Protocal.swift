//
//  Protocal.swift
//  MoneyBaseDemo
//
//  Created by Shailendra on 02/01/23.
//

import Foundation
import RxSwift
import RxCocoa

protocol APIHandlerDelegate {
    func callAPIFromApiHandler(url : String)-> Observable<Data?>
}
